<?php
    // Contenedor de servicios
    class App {
        private static $container = [];

        // Añade un servicio al contenedor
        public static function bind(string $key, $value) {
            static::$container[$key] = $value;
        }
        
        // Devuelve el servicio con la clave "$key"
        public static function get(string $key) {
            if (!array_key_exists($key, static::$container)) {
                throw new AppException("No se ha encontrado la clave $key en el contenedor.");
            }

            return static::$container[$key];
        }

        // Devuelve la conexión con la base de datos
        public static function getConnection() {
            if (!array_key_exists("connection", static::$container)) {
                static::$container["connection"] = Connection::make();
            }

            return static::$container["connection"];
        }
    }
?>