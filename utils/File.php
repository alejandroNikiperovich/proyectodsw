<?php
    // Archivos subidos al servidor
    class File {
        // Archivo
        private $file;

        // Nombre del archivo
        private $fileName;

        // Constructor
        public function __construct(string $fileName, array $arrTypes) {
            $this->file = $_FILES[$fileName];
            $this->fileName = $this->file["name"];

            // Comprobamos que existe el archivo
            if ($this->file["name"] == "") {
                throw new FileException("Debes especificar un fichero.");
            }

            // Comprobamos que se ha subido correctamente
            if ($this->file["error"] !== UPLOAD_ERR_OK) {
                switch ($this->file["error"]) {
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new FileException("El archivo es demasiado grande.");
                    break;
                    case UPLOAD_ERR_PARTIAL:
                        throw new FileException("El archivo no se ha subido por completo.");
                    break;
                    default:
                        throw new FileException("Hubo un problema al subir el archivo.");
                    break;
                }
            }

            // Comprobamos que no tiene una extensión no permitida
            if (in_array($this->file["type"], $arrTypes) === false) {
                throw new FileException("El tipo de archivo no está soportado.");
            }
        }

        // Guarda el archivo en la ruta "$rutaDestino"
        public function saveUploadFile($rutaDestino) {
            if (is_uploaded_file($this->file["tmp_name"])) {
                // Renombramos el archivo en case de que ya exista uno en el servidor con el mismo nombre
                if (is_file($rutaDestino . $this->fileName)) {
                    $this->file["name"] = time() . "_" . $this->file["name"];
                    $this->fileName = $this->file["name"];
                }

                if (!move_uploaded_file($this->file["tmp_name"], $rutaDestino . $this->file["name"])) {
                    throw new FileException("No se ha podido mover el fichero al destino especificado.");
                }
            } else {
                throw new FileException("El archivo no se ha subido mediante un formulario.");
            }
        }

        // Getter
        public function getFileName() {
            return $this->fileName;
        }
    }
?>