<?php require_once __DIR__ . "/partials/head.part.php"; ?>

<!-- ======= Section ======= -->
<section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2>Iniciar Sesión</h2>
        </div>

        <div class="row">

          <div class="col-lg-12" data-aos="fade-up" data-aos-delay="300">
            <form action="login" method="post" role="form">

              <div class="form-row">
                <!-- Nombre de usuario -->
                <div class="col-lg-6 form-group">
                  <input type="text" name="username" class="form-control" id="username" placeholder="Nombre de usuario" data-rule="required" data-msg="Campo obligatorio" value="<?= $username ?>"/>
                  <div class="validate"></div>
                </div>

                <!-- Contraseña -->
                <div class="col-lg-6 form-group">
                  <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" data-rule="required" data-msg="Campo obligatorio" />
                  <div class="validate"></div>
                </div>
              </div>
              
              <div class="mb-3">
                <div class="error-message">
                  <?php 
                    if ($_SERVER["REQUEST_METHOD"] === "POST" && !empty($errores)) {
                      foreach ($errores as $error) echo $error . "<br>";
                    }
                  ?>
                </div>
                <div class="sent-message">
                    <?php
                      if ($_SERVER["REQUEST_METHOD"] === "POST" && empty($errores)) {
                        echo "Sesión iniciada!";
                      }
                    ?>
                </div>
              </div>
              <div class="text-center"><button type="submit">Iniciar sesión</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Section -->

<?php require_once __DIR__ . "/partials/footer.part.php"; ?>