<?php
    // QueryBuilder para la tabla "usuarios"
    class UsuarioRepository extends QueryBuilder {
        public function __construct(string $table = "usuarios", string $classEntity = "Usuario") {
            parent::__construct($table, $classEntity);
        }

        // Encuentra un usuario según su nombre de usuario
        public function findUser(string $username) : IEntity {
            $sql = "SELECT * FROM usuarios WHERE username = '$username'";
            $result = $this->executeQuery($sql);

            if (empty($result)) {
                throw new NotFoundException("No se ha encontrade el usuario con nombre $username");
            }

            return $result[0];
        }
    }
?>