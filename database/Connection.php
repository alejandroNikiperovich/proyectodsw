<?php
    // Realiza una conexión con la base de datos mediante el fichero de configuración
    class Connection {
        public static function make() {
            try {
                $config = App::get("config");
                $connection = new PDO(
                    $config["connection"] . ";dbname=" . $config["name"],
                    $config["username"],
                    $config["password"],
                    $config["options"]
                );
            } catch (PDOException $PDOException) {
                throw new AppException("No se ha podido conectar con la BBDD.");
            }

            return $connection;
        }
    }
?>