<?php
    // Inyección de dependencias
    abstract class QueryBuilder {
        private $connection;
        private $table;
        private $classEntity;

        // Conectamos con la base de datos y guardamos una de sus tablas y el objeto correspondiente
        public function __construct(string $table, string $classEntity) {
            $this->connection = App::getConnection();
            $this->table = $table;
            $this->classEntity = $classEntity;
        }

        // Devuelve todos los registros de la tabla
        public function findAll() {
            $sql = "SELECT * FROM $this->table";
            $result = $this->executeQuery($sql);

            return $result;
        }

        // Inserta un registro nuevo en la tabla
        public function save(IEntity $entity) : void {
            try {
                // Cogemos los valores a insertar
                $parameters = $entity->toArray();

                // Creamos la sentencia insert con los valores
                $sql = sprintf("INSERT INTO %s (%s) VALUES (%s)",
                $this->table,
                implode(", ", array_keys($parameters)),
                ":" . implode(", :", array_keys($parameters))
                );

                // Preparamos la sentencia
                // Control inyección SQL
                $statement = $this->connection->prepare($sql);

                // Ejecuta la sentencia
                $statement->execute($parameters);
            } catch (PDOException $exception) {
                throw new QueryException("Error al insertar en la BBDD.");
            }
        }

        // Pasamos una consulta "$sql" y devuelve todos los registros encontrados como objetos "$classEntity"
        public function executeQuery(string $sql) : array {
            $pdoStatement = $this->connection->prepare($sql);

            if ($pdoStatement->execute() === false) {
                throw new QueryException("No se ha podido ejecutar la consulta.");
            }

            return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity);
        }

        // Encuentra un registro a partir de su id y lo devuelve como un objeto "$classEntity"
        public function find(int $id) : IEntity {
            $sql = "SELECT * FROM $this->table WHERE id = $id";
            $result = $this->executeQuery($sql);

            if (empty($result)) {
                throw new NotFoundException("No se ha encontrade el elemento con id $id");
            }

            return $result[0];
        }
    }
?>