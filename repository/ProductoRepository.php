<?php
    // QueryBuilder para la tabla "productos"
    class ProductoRepository extends QueryBuilder {
        public function __construct(string $table = "productos", string $classEntity = "Producto") {
            parent::__construct($table, $classEntity);
        }

        // Devuelve la categoría relacionada al producto
        public function getCategoria(Producto $producto): Categoria {
            $categoriaRepository = new CategoriaRepository();
            return $categoriaRepository->find($producto->getCategoria());
        }
    }
?>