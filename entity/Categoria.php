<?php
    // Categoría de un producto
    // Usado para filtros en la página "Productos"
    class Categoria implements IEntity {
        private $id;
        private $nombre;

        // Constructor
        // Solo se utiliza al ser llamado por CategoriaRepository (QueryBuilder)
        public function __construct($id = "", $nombre = "") {
            $this->id = $id;
            $this->nombre = $nombre;
        }

        public function toArray() : array {
            return [
                "nombre" => $this->getNombre()
            ];
        }

        // Getters
        public function getId() {
            return $this->id;
        }

        public function getNombre() {
            return $this->nombre;
        }
    }
?>