<!-- Cambiamos la categoría para que funcione el filtro de JQuery -->
<div class="col-lg-4 col-md-6 portfolio-item filter-<?= strtolower($builderProducto->getCategoria($producto)->getNombre()); ?>">
    <div class="portfolio-wrap">
    <img src="<?= $producto->getURLImagen(); ?>" class="img-fluid" alt="<?= $producto->getNombreProducto(); ?>">
    <div class="portfolio-info">
        <h4><?= $producto->getNombreProducto(); ?></h4>
        <p><?= $producto->getPrecio() . "€"; ?></p>
        <div class="portfolio-links">
        <a href="<?= $producto->getURLImagen(); ?>" data-gall="portfolioGallery" class="venobox" title="<?= $producto->getDescripcion(); ?>"><i class="icofont-eye"></i></a>
    </div>
    </div>
    </div>
</div>