<?php
    // Controlador frontal
    require_once "core/bootstrap.php";

    $routes = require_once "app/routes.php";

    require_once $routes[Request::uri()];
?>