<?php
    // No permitimos entrar a la página si no eres admin
    if (isset($_SESSION["username"])) {
        if ($_SESSION["rol"] != "Admin") {
            header("Location: home");
        }
    } else {
        header("Location: home");
    }

    try {
        // Variables para los inputs de los formularios
        $nombre = "";
        $precio = "";
        $categoria = "";
        $imagen = "";
        $descripcion = "";

        // Variable para mostrar errores
        $errores = array();

        // Variable para extensiones de archivo permitidas
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $builderProducto = new ProductoRepository();
        $builderCategoria = new CategoriaRepository();
        
        // Comprobamos que se ha enviado el formulario
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            try {
                // Error si no se ha rellenado algún formulario
                if ($_POST["nombre"] == "" || $_POST["precio"] == "" || $_POST["descripcion"] == "") {
                    $errores[] = "Todos los campos son obligatorios";
                }

                // Error si el campo "precio" no es un número
                if (!is_numeric($_POST["precio"])) {
                    $errores[] = "Precio no es un número";
                }

                // Guardamos los valores enviados por formulario
                // Control de inyección HTML
                $nombre = trim(htmlspecialchars($_POST["nombre"]));
                $precio = trim(htmlspecialchars($_POST["precio"]));
                $precio = floatval($precio); // Pasamos a float porque así está en la BBDD
                $categoria = trim(htmlspecialchars($_POST["categoria"]));
                $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

                // Guardamos la imagen y la subimos al servidor
                $imagen = new File("imagen", $tiposAceptados);
                $imagen->saveUploadFile(Producto::RUTA_IMAGENES);

                // Creamos el producto y lo insertamos en la BBDD
                if (empty($errores)) {
                    $producto = new Producto(0, $nombre, $imagen->getFilename(), $categoria, $precio, $descripcion);
                    $builderProducto->save($producto);
                    header("Location: products");
                }
            } catch (FileException $fileException) {
                $errores[] = $fileException->getMessage();
            }
        }

        // Guardamos las categorías para pasárselas a la vista
        $categorias = $builderCategoria->findAll();

        require_once  __DIR__ . "/../views/subir-producto.view.php";
    } catch (AppException $appException) {
        echo $appException->getMessage();
    }
?>