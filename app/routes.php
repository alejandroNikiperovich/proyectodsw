<?php
    // Rutas amigables
    return [
        "proyectoDSW/home" => "app/controllers/index.php",
        "proyectoDSW/products" => "app/controllers/products.php",
        "proyectoDSW/subir-producto" => "app/controllers/subir-producto.php",
        "proyectoDSW/contact" => "app/controllers/contact.php",
        "proyectoDSW/login" => "app/controllers/login.php",
        "proyectoDSW/logout" => "app/controllers/logout.php"
    ]
?>