<?php
    // No permitimos loguearnos si ya lo estamos
    if (isset($_SESSION["username"])) {
        header("Location: home");
    }

    $username = "";
    $errores = array();

    // Manipulamos el formulario
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        // Error si algunos campos no se han rellenado
        if ($_POST["username"] == "" || $_POST["password"] == "") {
            $errores[] = "Todos los campos son obligatorios";
        }

        // Evitamos inyección HTML
        $username = trim(htmlspecialchars($_POST["username"]));
        $password = trim(htmlspecialchars($_POST["password"]));

        try {
            // Encontramos al usuario según el username
            $builderUsuario = new UsuarioRepository();
            $usuario = $builderUsuario->findUser($username);
            
            // Comprobamos que la contraseña es correcta
            // Guardamos el nombre de usuario y el rol en una sesión
            if ($usuario->validarUsuario($password)) {
                $_SESSION["username"] = $username;
                $_SESSION["rol"] = $usuario->getRol();
                header("Location: home");
            } else {
                $errores[] = "Usuario no encontrado";
            }
        } catch (NotFoundException $notFoundException) {
            echo $notFoundException->getMessage();
        }
    }

    require_once __DIR__ . "/../views/login.view.php";
?>