<?php
    // Pasamos todos los productos a la vista
    try {
        $builderProducto = new ProductoRepository();
        $productos = $builderProducto->findAll();
    } catch (AppException $appException) {
        echo $appException->getMessage();
    }

    require_once  __DIR__ . "/../views/products.view.php";
?>