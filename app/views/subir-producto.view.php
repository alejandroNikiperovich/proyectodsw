<?php require_once __DIR__ . "/partials/head.part.php"; ?>

<!-- ======= Section ======= -->
<section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2>Subir Producto</h2>
        </div>

        <div class="row">

          <div class="col-lg-12" data-aos="fade-up" data-aos-delay="300">
            <form action="subir-producto" method="post" role="form" enctype="multipart/form-data">

              <div class="form-row">
                <!-- Nombre del producto -->
                <div class="col-lg-6 form-group">
                  <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre del producto" data-rule="required" data-msg="Campo obligatorio" value="<?= $nombre ?>"/>
                  <div class="validate"></div>
                </div>

                <!-- Precio -->
                <div class="col-lg-6 form-group">
                  <input type="text" class="form-control" name="precio" id="precio" placeholder="Precio (usa un punto para decimales)" data-rule="required" data-msg="Campo obligatorio" value="<?= $precio ?>"/>
                  <div class="validate"></div>
                </div>
              </div>

              <div class="form-row">
                <!-- Categoría del producto -->
                <div class="col-lg-6 form-group">
                  <select class="form-control" name="categoria">
                    <?php foreach ($categorias as $categoria) : ?>
                        <option value="<?= $categoria->getId()?>"><?= $categoria->getNombre();?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <!-- Imagen -->
                <div class="col-lg-6 form-group">
                  <input class="form-control-file" name="imagen" type="file">
                </div>
              </div>

              <!-- Descripción -->
              <div class="form-group">
                <textarea class="form-control" name="descripcion" rows="5" data-rule="required" data-msg="Campo obligatorio" placeholder="Descripción"><?= $descripcion ?></textarea>
                <div class="validate"></div>
              </div>
              
              <div class="mb-3">
                <div class="error-message">
                  <?php 
                    if ($_SERVER["REQUEST_METHOD"] === "POST" && !empty($errores)) {
                      foreach ($errores as $error) echo $error . "<br>";
                    }
                  ?>
                </div>
                <div class="sent-message">
                    <?php
                      if ($_SERVER["REQUEST_METHOD"] === "POST" && empty($errores)) {
                        echo "Producto subido!";
                      }
                    ?>
                </div>
              </div>
              <div class="text-center"><button type="submit">Subir producto</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Section -->

<?php require_once __DIR__ . "/partials/footer.part.php"; ?>