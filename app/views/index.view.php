<?php require_once __DIR__ . "/partials/head.part.php"; ?>

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <!-- Slide 1 -->
          <div class="carousel-item active" style="background-image: url('img/slide/slide1.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">Bienvenido a <span>Eustakiomponentes</span></h2>
                <p class="animate__animated animate__fadeInUp">Aseguramos que encontrarás cualquier producto informático más viejo que Eustakio</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Leer más</a>
              </div>
            </div>
          </div>

          <!-- Slide 2 -->
          <div class="carousel-item" style="background-image: url('img/slide/slide2.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">La BD humana</h2>
                <p class="animate__animated animate__fadeInUp">Anselmo es todavía más viejo y padece de Alzheimer. Lo único de lo que se acuerda es de todos números de serie de los componentes que vendemos</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Leer más</a>
              </div>
            </div>
          </div>

          <!-- Slide 3 -->
          <div class="carousel-item" style="background-image: url('img/slide/slide3.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">¿No encuentras algo?</h2>
                <p class="animate__animated animate__fadeInUp">Eso es que es más joven que Eustakio (no mostramos la edad de Eustakio por motivos legales)</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Leer más</a>
              </div>
            </div>
          </div>

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
          <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
          <span class="sr-only">Siguiente</span>
        </a>

      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row no-gutters">
          <div class="col-lg-6 video-box">
            <img src="img/about.jpg" class="img-fluid" alt="">
            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center about-content">

            <div class="section-title">
              <h2>Sobre nosotros</h2>
              <p>Eustakio compró un almacén en una subasta pensando que tendría oro. Resulta que solo tenía miles de componentes electrónicos descartados</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-fingerprint"></i></div>
              <h4 class="title"><a href="">Segunda Mano</a></h4>
              <p class="description">La mayoría probablemente ya hayan sido usados, algunos incluso no funcionan</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">El regalo perfecto</a></h4>
              <p class="description">Nada mejor que un ordenador de tubos de vacío para el aniversario de vuestro matrimonio</p>
            </div>

          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->

  </main><!-- End #main -->

<?php require_once __DIR__ . "/partials/footer.part.php"; ?>