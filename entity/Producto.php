<?php
    // Productos que se venden
    // Se muestran en la página "Productos"
    class Producto implements IEntity {
        // Ruta donde se guardan las imágenes de cada producto
        const RUTA_IMAGENES = "img/portfolio/";

        // Variables relacionadas con las columnas de la tabla "productos" de la base de datos
        private $id;
        private $nombreProducto;
        private $nombreImagen;
        private $categoria;
        private $precio;
        private $descripcion;

        // Constructor
        // Solo se utiliza al ser llamado por ProductoRepository (QueryBuilder)
        public function __construct($id = "", $nombreProducto = "", $nombreImagen = "", $categoria = "", $precio = 0, $descripcion = "") {
            $this->id = $id;
            $this->nombreProducto = $nombreProducto;
            $this->nombreImagen = $nombreImagen;
            $this->categoria = $categoria;
            $this->precio = $precio;
            $this->descripcion = $descripcion;
        }

        public function toArray() : array {
            return [
                "nombreProducto" => $this->getNombreProducto(),
                "nombreImagen" => $this->getNombreImagen(),
                "categoria" => $this->getCategoria(),
                "precio" => $this->getPrecio(),
                "descripcion" => $this->getDescripcion()
            ];
        }

        // Devuelve la ruta en el servidor de la imagen del producto
        public function getURLImagen() : string {
            return self::RUTA_IMAGENES . $this->getNombreImagen();
        }

        // Getters
        public function getId() {
            return $this->id;
        }

        public function getNombreProducto() {
            return $this->nombreProducto;
        }

        public function getNombreImagen() {
            return $this->nombreImagen;
        }

        public function getCategoria() {
            return $this->categoria;
        }

        public function getPrecio() {
            return $this->precio;
        }

        public function getDescripcion() {
            return $this->descripcion;
        }
    }
?>