<?php
    class Usuario implements IEntity {
        private $id;
        private $username;
        private $password;
        private $rol;

        // Constructor
        public function __construct($id = "", $username = "", $password = "", $rol = "") {
            $this->id = $id;
            $this->username = $username;
            $this->password = $password;
            $this->rol = $rol;
        }

        public function toArray() : array {
            return [
                "username" => $this->getUsername(),
                "password" => $this->getPassword(),
                "rol" => $this->getRol()
            ];
        }

        // Compara "pass" con la contraseña del usuario
        public function validarUsuario(string $pass) {
            if ($pass == $this->getPassword()) {
                return true;
            } else {
                return false;
            }
        }

        // Getters
        public function getId() {
            return $this->id;
        }

        public function getUsername() {
            return $this->username;
        }

        public function getPassword() {
            return $this->password;
        }

        public function getRol() {
            return $this->rol;
        }
    }
?>