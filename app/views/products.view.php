<?php require_once __DIR__ . "/partials/head.part.php"; ?>

<!-- ======= Our Products Section ======= -->
<section id="portfolio" class="portfolio section-bg">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="section-title">
          <h2>Productos</h2>
          <p>Vamos subiendo nuevos productos a medida que nuestros mineros los rescatan. No aceptamos devoluciones por objetos abollados</p>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">Todos</li>
              <li data-filter=".filter-componente">Componentes</li>
              <li data-filter=".filter-periférico">Periféricos</li>
              <li data-filter=".filter-servicio">Servicios</li>
            </ul>
          </div>
        </div>

        <!-- Mostramos todos los productos -->
        <div class="row portfolio-container">
          <?php
            foreach($productos as $producto) {
              require __DIR__ . "/partials/single-product.part.php";
            }
          ?>
        </div>

      </div>
    </section><!-- End Our Products Section -->

<?php require_once __DIR__ . "/partials/footer.part.php"; ?>