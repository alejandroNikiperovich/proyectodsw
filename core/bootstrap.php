<?php
    // Fichero de includes y configuración
    session_start();

    require_once "App.php";
    require_once "Request.php";

    require_once "database/Connection.php";
    require_once "database/IEntity.php";
    require_once "database/QueryBuilder.php";

    require_once "entity/Categoria.php";
    require_once "entity/Producto.php";
    require_once "entity/Usuario.php";

    require_once "exceptions/AppException.php";
    require_once "exceptions/FileException.php";
    require_once "exceptions/NotFoundException.php";
    require_once "exceptions/QueryException.php";

    require_once "repository/CategoriaRepository.php";
    require_once "repository/ProductoRepository.php";
    require_once "repository/UsuarioRepository.php";

    require_once "utils/File.php";

    $config = require_once __DIR__ . "/../app/config.php";
    App::bind("config", $config);
?>