<!-- ======= Footer ======= -->
<footer id="footer">
  <div class="footer-top">
    <div class="container">
      <div class="row">

        <div class="col-lg-3 col-md-6 footer-info">
          <h3>Eustakiomponentes</h3>
          <p>
            Finca del tío Pepe<br>
            España<br><br>
            <strong>Teléfono:</strong> +1 5589 55488 55<br>
            <strong>Email:</strong> eustakio@email.com<br>
          </p>
        </div>

        <div class="col-lg-2 col-md-6 footer-links">
          <h4>Enlaces</h4>
          <ul>
            <li><i class="bx bx-chevron-right"></i> <a href="home">Página Principal</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="products">Productos</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Términos de servicio</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Política de privacidad</a></li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-6 footer-links">
          <h4>Productos</h4>
          <ul>
            <li><i class="bx bx-chevron-right"></i> <a href="products">Componentes</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="products">Periféricos</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="products">Servicios</a></li>
          </ul>
        </div>

        <div class="col-lg-4 col-md-6 footer-newsletter">
          <h4>Suscribirse</h4>
          <p>Suscríbete para recibir ofertas al momento</p>
          <form action="" method="post">
            <input type="email" name="email"><input type="submit" value="Suscribirse">
          </form>

        </div>

      </div>
    </div>
  </div>

  <div class="container">
    <div class="copyright">
      &copy; Copyright <strong><span>Mamba</span></strong>. All Rights Reserved
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/mamba-one-page-bootstrap-template-free/ -->
      Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
    </div>
  </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/php-email-form/validate.js"></script>
<script src="vendor/jquery-sticky/jquery.sticky.js"></script>
<script src="vendor/venobox/venobox.min.js"></script>
<script src="vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="vendor/counterup/counterup.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="vendor/aos/aos.js"></script>

<!-- Template Main JS File -->
<script src="js/main.js"></script>

</body>

</html>